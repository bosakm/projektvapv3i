import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

import jdk.javadoc.internal.tool.Start;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Map;
import java.io.FileWriter;


public class Game {
    
    int WIN_SIZE_X = 1910;
    int WIN_SIZE_Y = 1010;

    public Map<String, JButton> dynamicButtons;
    
    JFrame frame;
    
    String EXPORT_FILE_NAME = "inventory";
    
    ArrayList<String> inventoryArray = new ArrayList<String>();
    
    int NUMBER_OF_VALUES = 1; //the number of items that you have in your file or values 
    
    
    
    public static void main(String[] args)
    {
        new Game();
    }
    
    
    public Game()
    {
        EventQueue.invokeLater(new Runnable() {
            public void run(){
                frame = new JFrame ("Game");
                
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                File inventory = new File(EXPORT_FILE_NAME);
                
                try {
                    
                    createEmptyArrayListIfNotExisting(inventory);
                    
                    makeScenes(null); // make

                } catch (IOException e) {
                    
                    e.printStackTrace();
                }
                
                
                frame.setLocationRelativeTo(null);
                frame.setResizable(true);
                frame.setVisible(true);
                
            }
        });
    }
    
    public void createEmptyArrayListIfNotExisting(File inventory) throws IOException
    {
        if (!inventory.exists()) {
            inventory.createNewFile();
            for (int i = 0; i < NUMBER_OF_VALUES; i++){
                inventoryArray.add("0");
            }
            
        } else {
            readFile(inventoryArray);
        }
    }

    public void resetGame() throws IOException
    {
        for (int i = 0; i<inventoryArray.size(); i++) {
            inventoryArray.set(i, "0");
        }

        writeFile(inventoryArray);
    }
    
    public void writeFile(ArrayList<String> list) throws IOException 
    {
        FileWriter writer = new FileWriter(EXPORT_FILE_NAME); 
        for(String str: list) {
            writer.write(str + System.lineSeparator());
        }
        writer.close();
    }
    
    public void readFile(ArrayList<String> list) throws IOException 
    {
        BufferedReader bufReader = new BufferedReader(new FileReader(EXPORT_FILE_NAME));
        
        String line = bufReader.readLine();
        while (line != null) {
            list.add(line);
            line = bufReader.readLine();
        }
        
        bufReader.close();
        
    }
    
    
    
    public void makeScenes(Scene curRoom) throws IOException 
    { // method for creating Scenes 
        Scene Room1 = new Scene("./img/scenes/room1.jpg");
        Scene Room2 = new Scene("./img/scenes/room2.jpg");
        Scene Hallway1 = new Scene("./img/scenes/hallway1.jpg");
        Scene Key1 = new Scene("./img/items/KeyShow.jpg");
        Scene Drawer = new Scene("./img/scenes/Drawer.jpg");
        Scene Gun = new Scene("./img/items/Gun.jpg");
        Scene WinScene = new Scene("./img/scenes/WinScene.jpg");
        
        
        // id pojmenovavej <odkud>-<kam> nebo <kde>-<akce>
        
        
        Room1.addButton(0,0,200,WIN_SIZE_Y, Hallway1, Room1, "Chodba", new String[]{"default"}, "Room1-Hallway1");
        Room1.addButton(850, 600, 50, 50, Key1, Room1, "klic",new String[]{"key1"}, "Room1-Key1");
        
        Room2.addButton(0,0,200,WIN_SIZE_Y, Hallway1, Room2, "Chodba",  new String[]{"default"}, "Room2-Hallway1");
        Room2.addButton(850, 800, 200, 100, Drawer, Room2, "Šuplík",new String[]{"dark"}, "Room2-Suplik");
        
        Hallway1.addButton(WIN_SIZE_X-400,0,160,WIN_SIZE_Y-100, Room1, Hallway1, "Pokoj 225", new String[]{"default"}, "Hallway1-Room1");
        Hallway1.addButton(250,120,150,WIN_SIZE_Y-100,Room2, Hallway1, "Pokoj 224", new String[]{"default"}, "Hallway1-Room2");
        Hallway1.addButton(610, 470, 100, 200, WinScene, Hallway1, "Dveře ven", new String[]{"default"}, "Hallway1-WinScene");
        
        Key1.addButton(0,0,200,200,Room1, Key1, "Dát do invetnáře a jít zpět", new String[]{"put-inv", "default"}, "Key1-Room1");
        
        Drawer.addButton(0, 0, 200, 200, Room2, Drawer, "Jít zpět", new String[]{"default"}, "Drawer-Back");
        Drawer.addButton(800, 250, 200, 200, Gun, Drawer, "Zbraň", new String[]{"default"}, "Drawer-Gun");

        WinScene.addButton(WIN_SIZE_X/2 - 500/2, 0, 500, 200, Room1, WinScene, "Resetovat hru", new String[]{"default"}, "Room1-Reset");
        
        Gun.addButton(0, 0, 200, 200, Drawer, Gun, "Jít zpět",new String[]{"default"} , "Gun-Drawer");
        
        
        if (curRoom == null){
            frame.add(Room1); //Initial Scene
        } else {
            frame.add(curRoom);
        }
        frame.pack();
    }
    
    public void changeScene(Scene thisScene,Scene nextScene)
    {
        frame.remove(thisScene);
        frame.add(nextScene);
        frame.pack();
        frame.repaint();        
    }
    
    public class Scene extends JPanel 
    {
        
        
        protected String env; // Environment takze backround hry ta fotka
        protected long StartTime;
        
        
        public Scene(String env) {
            this.setLayout(null);
            this.env = env;
        }
        
        public void addButton(int x, 
        int y, 
        int width, 
        int height, 
        Scene nextScene, 
        Scene thisScene, 
        String text, 
        String [] cls /*bude fungovat podobne jako class v html*/, 
        String uniqueName /*must be unique neco jako id v html*/) 
        {
            JButton bt = new JButton(text);
            
            for (int i = 0; i<cls.length; i++) {
                switch (cls[i]){
                    case "key1":
                    bt.setIcon(new ImageIcon("./img/effects/shinesmall.gif"));
                    bt.setBorder(null);
                    break;
                    case "default":
                    bt.setBorder(new LineBorder(Color.WHITE));
                    bt.setForeground(Color.WHITE);
                    break;
                    case "dark":
                    bt.setBorder(new LineBorder(Color.BLACK, 3));
                    bt.setFont(new Font("Serif", Font.BOLD, 30));
                    break;
                    default:
                    break;
                }
                
            }
            
            
            
            bt.setBounds(x,y,width,height);
            bt.setContentAreaFilled(false);
            
            
            bt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e){
                    
                    switch (uniqueName) {
                        case "Room1-Key1":
                            inventoryArray.set(0, "1"); 
                            System.out.println(inventoryArray.toString());
                            try {
                                writeFile(inventoryArray);
                                frame.remove(thisScene);
                                makeScenes(nextScene);
                                
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            break;
                        case "Key1-Room1":
                            if (inventoryArray.get(0) == "1") {
                                env = "./img/scenes/itemBackground.jpg";
                            } else {
                                env = "./img/items/KeyShow.jpg";
                            }
                        case "Hallway1-Room2":
                        if (inventoryArray.get(0) == "1") {
                                changeScene(thisScene, nextScene);
                            } else {
                                JOptionPane.showMessageDialog(null, "Nemáš klíč");
                            }
                            break;
                            case "Room1-Reset":
                            try {resetGame();} catch (IOException e1) {e1.printStackTrace();}
                            changeScene(thisScene, nextScene);
                            StartTime = System.currentTimeMillis();
                            break;
                        case "Hallway1-WinScene":
                        String kod = JOptionPane.showInputDialog(frame, "Zadejte kód");
                        if (kod.equals("60717659")) {
                                changeScene(thisScene, nextScene);
                                System.out.println(System.currentTimeMillis() - StartTime);
                            }
                            break;
                            default:
                            changeScene(thisScene, nextScene);
                            break;
                            
                        }
                        
                }
             
            });

            //dynamicButtons.put(uniqueName, bt); // adds this button to a map that stores the buttons gets called by unique name 
            this.add(bt);
            this.invalidate();                  
        }

        public void removeButton(String name) {
            JButton b = dynamicButtons.remove(name);
            this.remove(b);
            this.invalidate();
        }

        public Dimension getPreferredSize() {
            return new Dimension(WIN_SIZE_X, WIN_SIZE_Y);
        }

        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            
            try {
                g2d.drawImage(ImageIO.read(new File(env)), 0, 0, WIN_SIZE_X, WIN_SIZE_Y,null); // draws out background image
                g2d.drawImage(ImageIO.read(new File("./img/icon/hotbar.png")), 700, WIN_SIZE_Y-66, 676, 66, null);
                if(inventoryArray.get(0) == "1") { // if key is in inventory array
                    g2d.drawImage(ImageIO.read(new File("./img/icon/key-png-11.png")), 800, WIN_SIZE_Y-57, 50, 50, null); // draws
                }
                frame.repaint();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
